<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class InvoicesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Invoice');
        for ($i=0; $i < 10; $i++) { 
            DB::table('invoices')->insert([
                'date' => $faker->date,
                'customerID' => $faker->numberBetween($min = 1, $max = 7),
                'productID' => $faker->numberBetween($min = 1, $max = 5),
                'created_at'=>$faker->date,
                'updated_at'=>$faker->date,
            ]);
        }
    }
}
