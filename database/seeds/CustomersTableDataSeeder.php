<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class CustomersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        $faker = Faker::create('App\Customer');
        for ($i=0; $i < 10; $i++) { 
            DB::table('customers')->insert([
                'firstname' => $faker->firstname,
                'lastname' =>  $faker->lastname,
                'email' => $faker->unique()->safeEmail,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'created_at'=>$faker->date,
                'updated_at'=>$faker->date,
            ]);
        }
    }
}
