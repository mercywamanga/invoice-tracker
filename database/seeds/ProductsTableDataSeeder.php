<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class ProductsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Product');
        for ($i=0; $i < 10; $i++) { 
            DB::table('products')->insert([
                'name' => $faker->word,
                'price' => $faker->numberBetween($min = 1500, $max = 6000),
                'description' => $faker->sentence, 
                'created_at'=>$faker->date,
                'updated_at'=>$faker->date,  
            ]);
        }


    }
}
