<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    //testing listing of customers in the specific route
    public function testDisplayCustomerList()
    {
      $response=$this->json('GET','/api/customers',['id'=>1,'firstname'=>'mercy','lastname'=>'wamanga',
      'email'=>'tashma@gmail.com','address'=>'12345']);
      
      $response->assertStatus(200);
    }

    //testing listing of products in the specific route
    public function testDisplayProductList()
    {
      $response=$this->json('GET','/api/products',['id'=>1,'name'=>'campus','price'=>2000.00,
      'description'=>'tashmavyuyjvuyvgmailbkiiom','service'=>false]);
      $response->assertStatus(200);
    }

      //testing listing of invoices in the specific route
      public function testDisplayInvoiceList()
      {
        $response=$this->json('GET','/api/invoices',['id'=>1,'date'=>'12-dec-2001','productID'=>4,
        'customerID'=>10]);
        $response->assertStatus(200);
      }


    //testing creating of customers in the specific route
    public function testCreateCustomer()
    {
      $response=$this->json('POST','/api/customers',['id'=>1,'firstname'=>'merch','lastname'=>'wamanga',
      'email'=>'tashma15v29@gmail.com','address'=>'12345']);
      $response->assertOk();
    }

     //testing creating of products in the specific route
     public function testCreateProduct()
     {
       $response=$this->json('POST','/api/products',
       ['id'=>1,'name'=>'campus','price'=>2000.00,
       'description'=>'tashmavyuyjvuyvgmailbkiiom','service'=>false]);
       $response->assertStatus(200);
     }

      //testing creating of an Invoice in the specific route
      public function TestCreateInvoice()
      {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->post('/api/invoices', ['id'=>1,'date'=>'2004-08-17','productID'=>4,
        'customerID'=>10]);
        $response->assertStatus(700);
      }

 }

