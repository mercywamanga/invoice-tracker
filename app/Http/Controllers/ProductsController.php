<?php

namespace App\Http\Controllers;
use App\Http\Resources\Product as ProductResource;
use App\Product;
use App\Http\Requests;
use Validator;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get products.
        $products=Product::paginate(5);

        //Return collection of products as a resource.
        return ProductResource::collection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate product fields.
        $validator = Validator::make($request->json()->all(), [
            "name"=>"required",
            "price"=>"required",
            "service"=>"required"
        ]);

        if($validator->fails()){
                return response()->json($validator->errors()); 
        }
        else{
                //Add a product
        $product = new Product();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->service = $request->input('service');
        $productData=new ProductResource($product);
            if($productData->save()){
                return response()->json([
                    "message" =>  'Product created successfully',
                    "data" => $productData,
                ]);
            }else{
                return response()->json([
                    "message" =>  'Something went wrong!!!!'
                ]);
            }

        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //Find a specific product id and check if the id exists.
        $product=Product::find($id);
        if(is_null($product)){
            return response()->json(['message'=>'Record not found'],404);
        }
        else{
             //Return a single Product as a resource 
        return new ProductResource($product);
        }

       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //Check if Product id exists.
         $product=Product::find($id);
         if(is_null($product)){
             return response()->json(['message'=>'Record not found'],404);
         }

        //Validation of customer fields.
        $validator = Validator::make($request->json()->all(), [
            "name"=>"required",
            "price"=>"required",
            "service"=>"required"
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422); 
        }
        else{
                //Fields for update
            $product->name = $request->input('name');
            $product->price = $request->input('price');
            $product->description = $request->input('description');
            $product->service = $request->input('service');
            $productData=new ProductResource($product);
         if($productData->save()){

            //Return updated resource
            return response()->json([
                "message" =>  'Updated created successfully',
                "data" => $productData
            ]);
                    
            }else{
                return response()->json(['message'=>'Something went wrong!!!!']);
             }

        }
 
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //Find a specific product id and check if the id exists.
         $product=Product::find($id);
         if(is_null($product)){
            return response()->json(['message'=>'Record not found'],404);
        }
        else{
            if($product->delete()){
                return response()->json(['message'=>'Product deleted successfully']);
            }else{
                
                return response()->json(['message'=>'Something went wrong!!!!']);
            }
        }

        
    }
}
