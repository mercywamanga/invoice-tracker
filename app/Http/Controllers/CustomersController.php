<?php

namespace App\Http\Controllers;
use App\Http\Resources\Customer as CustomerResource;
use App\Customer;
use App\Http\Requests;
use Validator;


use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get customers.
        $customers=Customer::paginate(5);

        //Return collection of products as a resource.
        return CustomerResource::collection($customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate customer fields.
        $validator = Validator::make($request->json()->all(), [
            "firstname"=>"required",
            "lastname"=>"required",
            "email"=>"required|email|unique:customers,email"
        ]);

    if($validator->fails()){
         return response()->json($validator->errors()); 
     }
     else{
        //Add a customer.
        $customer = new Customer();
        $customer->firstname = $request->input('firstname');
        $customer->lastname = $request->input('lastname');
        $customer->email = $request->input('email');
        $customer->phone = $request->input('phone');
        $customer->address = $request->input('address');
        $customerData=new CustomerResource($customer);
            if($customerData->save()){
                return response()->json([
                    "message" =>  'Customer created successfully',
                    "data" => $customerData
                ]);
            }else{
                return response()->json([
                    "message" =>  'Something went wrong!!!!'
                ]);
            }
     }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Find a specific customer id.
        $customer=Customer::find($id);

        if(is_null($customer)){
            return response()->json(['message'=>'Record not found'],404);
        }
        else{
            
        //Return a single customer as a resource 
        return new CustomerResource($customer);

        }
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check if customer id exists.
        $customer=Customer::find($id);
        if(is_null($customer)){
            return response()->json(['message'=>'Record not found'],404);
        }

        //Validation of customer fields.
         $validator = Validator::make($request->json()->all(), [
            "firstname"=>"required",
            "lastname"=>"required",
            "email"=>"required|email"
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422); 
        }else{

        //Fields for update
        $customer->firstname = $request->input('firstname');
        $customer->lastname = $request->input('lastname');
        $customer->email = $request->input('email');
        $customer->phone = $request->input('phone');
        $customer->address = $request->input('address');
        $customerData=new CustomerResource($customer);
            if($customer->save()){
                return response()->json([
                    "message" =>  'Updated created successfully',
                    "data" => $customerData
                ]);
            }else{
                return response()->json(['message'=>'Something went wrong!!!!']);
            }

     }

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //Find a specific customer id.
        $customer=Customer::find($id);

        if(is_null($customer)){
            return response()->json(['message'=>'Record not found'],404);
        }
        else{
            if($customer->delete()){
                return response()->json(['message'=>'Customer deleted successfully']);
            }else{
                return response()->json(['message'=>'Something went wrong!!!!']);
            }
        }

    }
}
