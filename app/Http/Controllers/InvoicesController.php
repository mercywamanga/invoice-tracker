<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Invoice;
use Validator;

use Illuminate\Http\Request;

class InvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get invoices.
        $invoices=Invoice::paginate(5);

        //Return collection of invoices as a resource.
        return InvoiceResource::collection($invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate invoice fields.
        $validator = Validator::make($request->json()->all(), [
            "date"=>"required",
            "customerID"=>"required",
            "productID"=>"required"
        ]);

        if($validator->fails()){
                return response()->json($validator->errors(),422); 
        } 
        else{

            //Add an Invoice
        $invoice = new Invoice();
        $invoice->date = $request->input('date');
        $invoice->customerID = $request->input('customerID');
        $invoice->productID = $request->input('productID');
       
        $invoiceData=new InvoiceResource($invoice);
            if($invoiceData->save()){
                return response()->json([
                    "message" =>  'Invoice created successfully',
                    "data" => $invoiceData
                ]);
            }else{
                return response()->json('Something went wrong!!!!');
            }

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //Find a specific invoice id and check if the id exists.
         $invoice=Invoice::find($id);
         if(is_null($invoice)){
             return response()->json(['message'=>'Record not found'],404);
         }
         else{
             //Return a single customer as a resource 
             return new InvoiceResource(Invoice::findOrFail($id));
         }
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check if invoice id exists.
        $invoice=Invoice::find($id);
        if(is_null($invoice)){
            return response()->json(['message'=>'Record not found'],404);
        }

        //Validation of invoice fields.
        $validator = Validator::make($request->json()->all(), [
            "date"=>"required",
            "customerID"=>"required",
            "productID"=>"required"
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422); 
        }
        else{

        //Fields for update
        $invoice->date = $request->input('date');
        $invoice->customerID = $request->input('customerID');
        $invoice->productID = $request->input('productID');
        $invoiceData=new InvoiceResource($invoice);
            if($invoiceData->save()){
            
                return response()->json([
                    "message" =>  'Updated created successfully',
                    "data" => $invoiceData
                ]);
                
            }else{
                return response()->json(['message'=>'Something went wrong!!!!']);
            }

        }

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find a specific invoice id and check if the id exists.
        $invoice=Invoice::find($id);
        if(is_null($invoice)){
           return response()->json(['message'=>'Record not found'],404);
       }
       else{
        if($invoice->delete()){
            return response()->json(['message'=>'invoice deleted successfully']);
        }else{
            
            return response()->json(['message'=>'Something went wrong!!!!']);
        }
       }

       
    }
}
