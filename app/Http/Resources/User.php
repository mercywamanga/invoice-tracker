<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'userID' => $this->userID,
            'name' => $this->name,
            'email' => $this->email,
            'email_verified_at'=> $this->email_verified_at,
            'password'=>$this->password,
            'remember_token'=>$this->remember_token,
        ];
    }
}
