# invoice-tracker

A simple laravel backend API application that the staff can use to create and track customer invoices.

## Getting started
1. Clone the application using  https://gitlab.com/mercywamanga/invoice-tracker.git.
2. Run php artisan migrate.
3. Run php artisan db:seed / php artisan make:seeder SeederName to run seeders.
4. Run php artisan serve.
5. In url enter - https:/localhost:8080/api/customers depending on your localhost port.
## Name
Invoice tracker System 

## Description
helps users easily track customers invoices efficiently.
Features Available
1. Add Customer,Product and invoices.
2. Update Customer,Product and invoices record.
3. Delete Customer,Product and invoices record.
4. Ability to view all records for  Customer,Product and invoices in the system
5. Has Validation of all required fields
6. Pagination for easy navigation


