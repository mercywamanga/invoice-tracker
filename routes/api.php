<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/*
  Customers Routes
*/
Route::resource('customers','CustomersController');


/*
  Products Routes
*/
Route::resource('products','ProductsController');


/*
  Invoices Routes
*/
Route::resource('invoices','InvoicesController');

/*
  Users Routes
*/
Route::resource('users','UsersController');